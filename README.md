
Blinker
=======

This is a simple application to be run on an ESP8266 (actually, a [Witty-cloud](https://www.instructables.com/ESP8266ESP12-Witty-Cloud-Arduino-Powered-SmartThin-1/) ) in order to generate **A LOT** of "blinks" (~ 110 per second)

I've created it in order to test how much performant an [Arduino Nano](https://www.arduino.cc/en/pmwiki.php?n=Main/ArduinoBoardNano) can be in "checking" such blinks, thanks to the infrared sensor [CNY70](https://duckduckgo.com/?q=cny70&t=h_&iax=images&ia=images).

And... if you're thinking: "What?" then...

Why I did it in such a way?
---------------------------
* I know that [interrupts](https://www.gammon.com.au/interrupts) are a **MUCH** better way to handle such a task but... on the board I'm going to connect the sensor (aka: a [ProDino Ethernet](https://kmpelectronics.eu/products/prodino-ethernet-v2/)) there are **NO** free PINs available (among the ones being able to handle interrupts)

* 'cause I was just curious to see how fast can be Arduino in "catching" even the smallest blink

* 'cause the power-meter counter I'll be interfacing with, will generate blinks as large as 5 ms (thanks to my friend Emilio, who measured it **exactly** with it's own oscilloscope!)

And... if you're curious...

I want to test it!
------------------
This is a [PlatformIO](https://platformio.org/) project. So it's strightforward to clone it, import it in PlatformIO, compile and flash...

I'm curious! I want more info!
-----------------------------
Feel free to contact me directly, preferibly on Telegram (@Damiano_Verzulli) or via some other means :-)
