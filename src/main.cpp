#include <Arduino.h>
#include <stdlib.h>

const int LDR = A0;
const int RED = D8;
const int GREEN = D6;
const int BLUE = D7;

unsigned long tsLedOn;
unsigned long tsCur;
unsigned long lastTs;
unsigned long lastPrint;

unsigned long counter;

int incomingByte = 0;

// LED will be "on", for `ledDuration` milliseconds
int ledDuration = 3;

// every `statsDelay` milliseconds, a stat counter will be
// displayed in the console
int statsDelay = 1000;

// after each blink, wait `ledBlinkDelay` milliseconds
// before next blink
int ledBlinkDelay = 5;

void setup()
{
  Serial.begin(115200);

  // initialize PINs
  // Please note that I'm running on a Witty-Cloud:
  // https://www.instructables.com/ESP8266ESP12-Witty-Cloud-Arduino-Powered-SmartThin-1/
  // so I have an LDR and an RGB [*] LED on-board
  // RGB: a single LED driven by _THREE_ distinct PINs

  pinMode(LDR, INPUT);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);

  // Let's initialize a few timestamp and counters
  tsLedOn = 0;
  counter = 0;
  lastTs = millis();
  lastPrint = lastTs;
}

void loop()
{
  // What time is is?
  tsCur = millis();

  // Let's see if it's time to output stats in console
  if (tsCur - lastPrint > statsDelay) {
    Serial.print("Number of blink sent: [");
    Serial.print(counter);
    Serial.println("]");

    // reset timer and counter
    lastPrint = tsCur;
    counter=0;
  }

  // Let's see if it's time to TURN-ON the LEd
  if (tsCur - lastTs > ledBlinkDelay) {
    counter++;

    // I'm choosing to turn on ONLY the RED component
    digitalWrite(RED, HIGH);
    // digitalWrite(GREEN, HIGH);
    // digitalWrite(BLUE, HIGH);

    // Let's keep in mind that the LED is "on"
    tsLedOn = tsCur;

    // ...and that we have turned it on, NOW.
    lastTs = tsCur;
  }

  // if led is ON, let's check if blinking time (ledDuration)
  // passed away... or not
  if ((tsLedOn > 0) && (tsCur - tsLedOn >= ledDuration)) {

    // It's passed away! Let's turn the LED OFF
    digitalWrite(RED, LOW);
    // digitalWrite(GREEN, LOW);
    // digitalWrite(BLUE, LOW);

    // Let's keep trach that we turned the led OFF
    tsLedOn = 0;

    // ..and we turned it off, _NOW_!
    lastTs = tsCur;
  }

  // We can send a single CHAR (0..9) via console 
  // in order to dinamically set ledDuration
  // (default is 4)

  // Let's see if something is arriving (serial buffer)
  if (Serial.available() > 0) {
    // Yes! Let's read it (it's a single char!)
    // ...and as it's an ASCII code, let's transform it
    // in a real INT
    incomingByte = Serial.read()-48; 

    // if it's something between 1 and 9, let's update
    // the ledDuration
    if (incomingByte >= 1 || incomingByte <= 9) {
      ledDuration = incomingByte;
      Serial.print("New ledDuration value is: ");
      Serial.print(ledDuration);
    }
  }
}